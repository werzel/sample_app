# Ruby on Rails Tutorial sample application

This is the sample application for
[*Ruby on Rails Tutorial:
Learn Web Development with Rails*](http://www.railstutorial.org/)
by [Michael Hartl](http://www.michaelhartl.com/).

## License

All source code in the [Ruby on Rails Tutorial](http://railstutorial.org/)
is available jointly under the MIT License and the Beerware License. See
[LICENSE.md](LICENSE.md) for details.

## Getting started

To get started with the app, clone the repo and then install the needed gems:

```
$ bundle install --without production
```

Next, migrate the database:

```
$ rails db:migrate
```

Finally, run the test suite to verify that everything is working correctly:

```
$ rails test
```

If the test suite passes, you'll be ready to run the app in a local server:

```
$ rails server
```

For more information, see the
[*Ruby on Rails Tutorial* book](http://www.railstutorial.org/book).


Exercicies

1.Assign variables city and state to your current city and state of residence. 

>> city="Guarapuava"
=> "Guarapuava"
>> state="Paraná"
=> "Paraná"
>> 

2.Using interpolation, print (using puts) a string consisting of the city and state separated by a comma and as space, as in “Los Angeles, CA”. 

>> puts "#{city},  #{state}"
Guarapuava,  Paraná
=> nil

3.Repeat the previous exercise but with the city and state separated by a tab character. 

>> puts "#{city}\t#{state}"
Guarapuava	Paraná
=> nil

4.What is the result if you replace double quotes with single quotes in the previous exercise? 
>> puts '#{city}\t#{state}'
#{city}\t#{state}
=> nil


1.What is the length of the string “racecar”? 

>> str="racecar"
=> "racecar"
>> str.length
=> 7

2.Confirm using the reverse method that the string in the previous exercise is the same when its letters are reversed. 

>> str.reverse
=> "racecar"

3.Assign the string “racecar” to the variable s. Confirm using the comparison operator == that s and s.reverse are equal. 

>> str==str.reverse
=> true

4.What is the result of running the code shown in Listing 4.9? How does it change if you reassign the variable s to the string “onomatopoeia”? Hint: Use up-arrow to retrieve and edit previous commands 

>> puts "It's a palindrome!" if str == str.reverse
It's a palindrome!
=> nil



1.By replacing FILL_IN with the appropriate comparison test shown in Listing 4.10, define a method for testing palindromes. Hint: Use the comparison shown in Listing 4.9. 

>> def palindrome_tester(s)
>>   if s==s.reverse
>>     puts "It's a palindrome!"
>>   else
?>     puts "It's not a palindrome."
>>   end
>> end
=> :palindrome_tester

2.By running your palindrome tester on “racecar” and “onomatopoeia”, confirm that the first is a palindrome and the second isn’t. 

?> palindrome_tester("racecar")
It's a palindrome!
=> nil
>> palindrome_tester("onomatopoeia")
It's not a palindrome.
=> nil

3.By calling the nil? method on palindrome_tester("racecar"), confirm that its return value is nil (i.e., calling nil? on the result of the method should return true). This is because the code in Listing 4.10 prints its responses instead of returning them.

>> palindrome_tester("racecar").nil?
It's a palindrome!
=> true


1.Assign a to be to the result of splitting the string “A man, a plan, a canal, Panama” on comma-space. 

>> a="A man, a plan, a canal, Panama".split(',')
=> ["A man", " a plan", " a canal", " Panama"]

2.Assign s to the string resulting from joining a on nothing.

>> s=a.join
=> "A man a plan a canal Panama"
>> 

3.Split s on whitespace and rejoin on nothing. Use the palindrome test from Listing 4.10 to confirm that the resulting string s is not a palindrome by the current definition. Using the downcase method, show that s.downcase is a palindrome. 

>> s.split(" ")
=> ["A", "man", "a", "plan", "a", "canal", "Panama"]
>> puts s
A man a plan a canal Panama
=> nil
>> puts s
A man a plan a canal Panama
=> nil
>> s=s.split(" ")
=> ["A", "man", "a", "plan", "a", "canal", "Panama"]
>> s=s.join
=> "AmanaplanacanalPanama"
>> palindrome_tester(s)
It's not a palindrome.
=> nil
>> palindrome_tester(s.downcase)
It's a palindrome!
=> nil

4.What is the result of selecting element 7 from the range of letters a through z? What about the same range reversed? Hint: In both cases you will have to convert the range to an array. 

>> a=('a'..'z').to_a
=> ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]
>> a[7]
=> "h"
>> a.reverse
=> ["z", "y", "x", "w", "v", "u", "t", "s", "r", "q", "p", "o", "n", "m", "l", "k", "j", "i", "h", "g", "f", "e", "d", "c", "b", "a"]
>> a.reverse!
=> ["z", "y", "x", "w", "v", "u", "t", "s", "r", "q", "p", "o", "n", "m", "l", "k", "j", "i", "h", "g", "f", "e", "d", "c", "b", "a"]
>> a[7]
=> "s"
>> 

















1.Using the range 0..16, print out the first 17 powers of 2. 

>> (0..16).each { |i| puts i * i }
0
1
4
9
16
25
36
49
64
81
100
121
144
169
196
225
256
=> 0..16
>> 

2.Define a method called yeller that takes in an array of characters and returns a string with an ALLCAPS version of the input. Verify that yeller([’o’, ’l’, ’d’]) returns "OLD". Hint: Combine map, upcase, and join.

>> def yeller(m)
>>   puts m.map { |char| char.upcase }.join
>> end
=> :yeller
>> yeller(['o', 'l', 'd'])
OLD
=> nil

3.Define a method called random_subdomain that returns a randomly generated string of eight letters.

>> def random_subdomain()
>>   puts ('a'..'z').to_a.shuffle[0..7].join
>> end
=> :random_subdomain
>> random_subdomain
vsioyguc
=> nil







4.By replacing the question marks in Listing 4.12 with the appropriate methods, combine split, shuffle, and join to write a function that shuffles the letters in a given string. 

>> def string_shuffle(s)
>>   puts s.split('').shuffle.join
>> end
=> :string_shuffle
>> string_shuffle("foobar")
orbfoa
=> nil


1.Define a hash with the keys ’one’, ’two’, and ’three’, and the values ’uno’, ’dos’, and ’tres’. Iterate over the hash, and for each key/value pair print out "’#{key}’ in Spanish is ’#{value}’". 

>> arr={one:"uno",two:"dos",three:"tres"}
=> {:one=>"uno", :two=>"dos", :three=>"tres"}
>> arr.each do |key,value|
?>  puts "#{key} in Spanish is #{value}"
>> end
one in Spanish is uno
two in Spanish is dos
three in Spanish is tres
=> {:one=>"uno", :two=>"dos", :three=>"tres"}
>> 

2.Create three hashes called person1, person2, and person3, with first and last names under the keys :first and :last. Then create a params hash so that params[:father] is person1, params[:mother] is person2, and params[:child] is person3. Verify that, for example, params[:father][:first] has the right value. 

>> person1={first:"ricardo", last:"werzel"}
=> {:first=>"ricardo", :last=>"werzel"}
>> person2={first:"tatiane",last:"dias alves werzel"}
=> {:first=>"tatiane", :last=>"dias alves werzel"}
>> person3={first:"ricardo",last:"werzel filho"}
=> {:first=>"ricardo", :last=>"werzel filho"}
>> params={}
=> {}
>> params[:father]=person1
=> {:first=>"ricardo", :last=>"werzel"}
>> params[:mother]=person2
=> {:first=>"tatiane", :last=>"dias alves werzel"}
>> params[:child]=person3
=> {:first=>"ricardo", :last=>"werzel filho"}
>>  params[:father][:first]
=> "ricardo"




3.Define a hash with symbol keys corresponding to name, email, and a “password digest”, and values equal to your name, your email address, and a random string of 16 lower-case letters. 

=> "ricardo"
>> hash={name:"ricardo", email:"werzel_r@hotmail.com"}
=> {:name=>"ricardo", :email=>"werzel_r@hotmail.com"}
>> hash[:password]="#{hash[:name]}#{hash[:email]}#{('a'..'z').to_a.shuffle[0..15].join}"
=> "ricardowerzel_r@hotmail.comqeytlnspxukjgrmo"
>> 

4.Find an online version of the Ruby API and read about the Hash method merge. What is the value of the following expression? 

>> { "a" => 100, "b" => 200 }.merge({ "b" => 300 })
=> {"a"=>100, "b"=>300}


1.What is the literal constructor for the range of integers from 1 to 10?
?> a=(1..10).to_a
=> [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
>> a.class
=> Array

2.What is the constructor using the Range class and the new method? Hint: new takes two arguments in this context.

>> r=Range.new(1,10)
=> 1..10

3.Confirm using the == operator that the literal and named constructors from the previous two exercises are identical. 

>> r==Range.new(1,10)
=> true


1.What is the class hierarchy for a range? For a hash? For a symbol?
>> r=Range.new(1,10)
=> 1..10
>> r.class
=> Range
>> r.class.superclass
=> Object
>> r.class.superclass.superclass
=> BasicObject
>> r.class.superclass.superclass.superclass
=> nil
>> 







2.Confirm that the method shown in Listing 4.15 works even if we replace 

>> class Word < String
>>  def palindrome?
>>    self == reverse
>>  end
>> end
=> :palindrome?
>> s = Word.new("level") 
=> "level"
>> s.palindrome?   
=> true
>> 


1.Verify that “racecar” is a palindrome and “onomatopoeia” is not. What about the name of the South Indian language “Malayalam”? Hint: Downcase it first. 

>> class String
>>   def palindrome?
>>     self == reverse
>>   end
>> end
=> :palindrome?
>> "racecar".palindrome?
=> true
>> "onomatopoeia".palindrome?
=> false
>> "Malayalam".downcase.palindrome?
=> true

2.Using Listing 4.16 as a guide, add a shuffle method to the String class. Hint: Refer to Listing 4.12. 

>> class String
>>   def shuffle
>>     self.split('').shuffle.join
>>   end
>> end
=> :shuffle

3.Verify that Listing 4.16 works even if you remove self..

>> class String
>>   def shuffle
>>     split('').shuffle.join
>>   end
>> end
=> :shuffle
>> "teste".shuffle
=> "tstee”

1.In the example User class, change from name to separate first and last name attributes, and then add a method called full_name that returns the first and last names separated by a space. Use it to replace the use of name in the formatted email method. 

class User
  attr_accessor :first, :last, :email

  def initialize(attributes = {})
    @first  = attributes[:first]
    @last  = attributes[:last]
    @email = attributes[:email]
  end

  def formatted_email
    "#{full_name} <#{@email}>"
  end

  def full_name
    "#{@first} <#{@last}>"
  end
end

>> require './example_user'
=> true
>> example = User.new(first:"ricardo", last:"werzel", email:"werzel_r@hotmail.com")
=> #<User:0x000000017dc9d0 @first="ricardo", @last="werzel", @email="werzel_r@hotmail.com">
>> example.formatted_email
=> "ricardo <werzel> <werzel_r@hotmail.com>"
>> 

2.Add a method called alphabetical_name that returns the last name and first name separated by comma-space.

class User
  attr_accessor :first, :last, :email

  def initialize(attributes = {})
    @first  = attributes[:first]
    @last  = attributes[:last]
    @email = attributes[:email]
  end

  def formatted_email
    "#{full_name} <#{@email}>"
  end

  def full_name
    "#{@first} #{@last}"
  end

  def alphabetical_name
    "#{@last}, #{@first}"
  end
  
end


>> require './example_user'
=> true
>> example = User.new(first:"ricardo", last:"werzel", email:"werzel_r@hotmail.com")
=> #<User:0x00000001b92e30 @first="ricardo", @last="werzel", @email="werzel_r@hotmail.com">
>> example.alphabetical_name
=> "werzel, ricardo"


3.Verify that full_name.split is the same as alphabetical_name.split(’, ’).reverse. 

>> example.full_name.split
=> ["ricardo", "werzel"]
>> example.alphabetical_name.split(',').reverse
=> [" ricardo", "werzel"]




capitulo 5

1.It’s well-known that no web page is complete without a cat image. Using the command in Listing 5.4, arrange to download the kitten pic shown in Figure 5.3.8

ricardo@ricardo-VirtualBox:~/workspace/sample_app$ curl -OL cdn.learnenough.com/kitten.jpg

2.Using the mv command, move kitten.jpg to the correct asset directory for images (Section 5.2.1). 
ricardo@ricardo-VirtualBox:~/workspace/sample_app$ mv kitten.jpg app/assets/images/

3.Using image_tag, add kitten.jpg to the Home page, as shown in Figure 5.4. 

<%= image_tag("kitten.jpg", alt: "kitten")%> 


1.Using code like that shown in Listing 5.10, comment out the cat image from Section 5.1.1.1. Verify using a web inspector that the HTML for the image no longer appears in the page source. 

<%#= image_tag("kitten.jpg", alt: "Kitten") %>






2.By adding the CSS in Listing 5.11 to custom.scss, hide all images in the application—currently just the Rails logo on the Home page). Verify with a web inspector that, although the image doesn’t appear, the HTML source is still present.

img {
  display: none;
}


1.Replace the default Rails head with the call to render shown in Listing 5.18. Hint: For convenience, cut the default header rather than just deleting it. 

<!DOCTYPE html>
<html>
  <head>
    <title><%= full_title(yield(:title)) %></title>
    <%= render 'layouts/rails_default' %>
    <%= render 'layouts/shim' %>
  </head>
  <body>
    <%= render 'layouts/header' %>
    <div class="container">
      <%= yield %>
      <%= render 'layouts/footer' %>
    </div>
  </body>
</html>


2.Because we haven’t yet created the partial needed by Listing 5.18, the tests should be red. Confirm that this is the case. 


ERROR["test_should_get_home", StaticPagesControllerTest, 6.23559889200078]
 test_should_get_home#StaticPagesControllerTest (6.24s)
ActionView::Template::Error:         ActionView::Template::Error: Missing partial layouts/_rails_default with {:locale=>[:en], :formats=>[:html], :variants=>[], :handlers=>[:raw, :erb, :html, :builder, :ruby, :coffee, :jbuilder]}. Searched in:
          * "/home/ricardo/workspace/sample_app/app/views"
        
            app/views/layouts/application.html.erb:5:in `_app_views_layouts_application_html_erb___2939429449331130440_51777480'
            test/controllers/static_pages_controller_test.rb:14:in `block in <class:StaticPagesControllerTest>'

  5/5: [===================================] 100% Time: 00:00:06, Time: 00:00:06

Finished in 6.24720s
5 tests, 0 assertions, 0 failures, 5 errors, 0 skips

ricardo@ricardo-VirtualBox:~/workspace/sample_app$ 


3.Create the necessary partial in the layouts directory, paste in the contents, and verify that the tests are now green again.

<%= csrf_meta_tags %>
<%= stylesheet_link_tag    'application', media: 'all',
                                              'data-turbolinks-track': 'reload' %>
<%= javascript_include_tag 'application', 'data-turbolinks-track': 'reload' %>

Started with run options --seed 7191

  5/5: [===================================] 100% Time: 00:00:00, Time: 00:00:00

Finished in 0.69389s
5 tests, 9 assertions, 0 failures, 0 errors, 0 skips



1.As suggested in Section 5.2.2, go through the steps to convert the footer CSS from Listing 5.17 to Listing 5.20 to SCSS by hand. 

footer {
  margin-top: 45px;
  padding-top: 5px;
  border-top: 1px solid #eaeaea;
  color: #777;
  a {
    color: #555;
    &:hover {
      color: #222;
    }
  }
  small {
    float: left;
  }
  ul {
    float: right;
    list-style: none;
    li {
      float: left;
      margin-left: 15px;
    }
  }
}


1.It’s possible to use a named route other than the default using the as: option. Drawing inspiration from this famous Far Side comic strip, change the route for the Help page to use helf. 

get  '/help',    to: 'static_pages#help', as: 'helf'






2.Confirm that the tests are now red. Get them to green by updating the route in Listing 5.28. 

ERROR["test_should_get_help", StaticPagesControllerTest, 0.4818574790006096]
 test_should_get_help#StaticPagesControllerTest (0.48s)
NameError:         NameError: undefined local variable or method `help_path' for #<StaticPagesControllerTest:0x00000006636fb8>
            test/controllers/static_pages_controller_test.rb:12:in `block in <class:StaticPagesControllerTest>'

  4/4: [===================================] 100% Time: 00:00:00, Time: 00:00:00

Finished in 0.54991s
4 tests, 6 assertions, 0 failures, 1 errors, 0 skips



1.In the footer partial, change about_path to contact_path and verify that the tests catch the error. 

FAIL["test_layout_links", SiteLayoutTest, 0.4346592510009941]
 test_layout_links#SiteLayoutTest (0.43s)
        Expected at least 1 element matching "a[href="/about"]", found 0..
        Expected 0 to be >= 1.
        test/integration/site_layout_test.rb:10:in `block in <class:SiteLayoutTest>'

  5/5: [===================================] 100% Time: 00:00:00, Time: 00:00:00

Finished in 0.52263s
5 tests, 12 assertions, 1 failures, 0 errors, 0 skips

2.It’s convenient to use the full_title helper in the tests by including the Application helper into the test helper, as shown in Listing 5.35. We can then test for the right title using code like Listing 5.36. This is brittle, though, because now any typo in the base title (such as “Ruby on Rails Tutoial”) won’t be caught by the test suite. Fix this problem by writing a direct test of the full_title helper, which involves creating a file to test the application helper and then filling in the code indicated with FILL_IN in Listing 5.37. (Listing 5.37 uses assert_equal <expected>, <actual>, which verifies that the expected result matches the actual value when compared with the == operator.) 

test "full title helper" do
    assert_equal full_title, "Ruby on Rails Tutorial Sample App"        
    assert_equal full_title("Help"), "Help | Ruby on Rails Tutorial Sample App"
  end


1.Per Table 5.1, change the route in Listing 5.41 to use signup_path instead of users_new_url.

test "should get new" do
    get signup_path
    assert_response :success
  end

2.The route in the previous exercise doesn’t yet exist, so confirm that the tests are now red. (This is intended help us get comfortable with the red/green flow of Test Driven Development (TDD, Box 3.3); we’ll get the tests back to green in Section 5.4.2.) 

ERROR["test_should_get_new", UsersControllerTest, 0.23034586499852594]
 test_should_get_new#UsersControllerTest (0.23s)
NameError:         NameError: undefined local variable or method `signup_path' for #<UsersControllerTest:0x00000003179280>
            test/controllers/users_controller_test.rb:5:in `block in <class:UsersControllerTest>'

  7/7: [===================================] 100% Time: 00:00:03, Time: 00:00:03

Finished in 3.96820s
7 tests, 16 assertions, 0 failures, 1 errors, 0 skips



1.If you didn’t solve the exercise in Section 5.4.1.1, change the test in Listing 5.41 to use the named route signup_path. Because of the route defined in Listing 5.43, this test should initially be green. 


2.In order to verify the correctness of the test in the previous exercise, comment out the signup route to get to red, then uncomment to get to green. 

ERROR["test_should_get_home", StaticPagesControllerTest, 0.7780269639988546]
 test_should_get_home#StaticPagesControllerTest (0.78s)
ActionView::Template::Error:         ActionView::Template::Error: undefined local variable or method `signup_path' for #<#<Class:0x000000040635e8>:0x000000066924f8>
            app/views/static_pages/home.html.erb:10:in `_app_views_static_pages_home_html_erb__2232204290120267591_53777720'
            test/controllers/static_pages_controller_test.rb:6:in `block in <class:StaticPagesControllerTest>'

  7/7: [===================================] 100% Time: 00:00:00, Time: 00:00:00

Finished in 0.82373s
7 tests, 8 assertions, 0 failures, 3 errors, 0 skips

 7/7: [===================================] 100% Time: 00:00:00, Time: 00:00:00

Finished in 0.51554s
7 tests, 17 assertions, 0 failures, 0 errors, 0 skips

3.In the integration test from Listing 5.32, add code to visit the signup page using the get method and verify that the resulting page title is correct. Hint: Use the full_title helper as in Listing 5.36. 

get signup
_path
    assert_select "title", full_title("Sign up")


CAPITULO 6 EXERCICIOS

1.Rails uses a file called schema.rb in the db/ directory to keep track of the structure of the database (called the schema, hence the filename). Examine your local copy of db/schema.rb and compare its contents to the migration code in Listing 6.2. 

as diferencas estao na versao com data e hora, no force: :cascade
e no t.datetime "created_at", null: false
    t.datetime "updated_at", null: false

2.After running this command, examine db/schema.rb to confirm that the rollback was successful. (See Box 3.1 for another technique useful for reversing migrations.) Under the hood, this command executes the drop_table command to remove the users table from the database. The reason this works is that the change method knows that drop_table is the inverse of create_table, which means that the rollback migration can be easily inferred. In the case of an irreversible migration, such as one to remove a database column, it is necessary to define separate up and down methods in place of the single change method. Read about migrations in the Rails Guides for more information. 

O seeds.rb ficou vazio, com versao 0

3.Re-run the migration by executing rails db:migrate again. Confirm that the contents of db/schema.rb have been restored. 

O seeds.rb voltou com os dados anterior.


1.In a Rails console, use the technique from Section 4.4.4 to confirm that User.new is of class User and inherits from ApplicationRecord. 
>> user.class.superclass
=> ApplicationRecord(abstract)

2.Confirm that ApplicationRecord inherits from ActiveRecord::Base. 

>> user.class.superclass.superclass
=> ActiveRecord::Base

1.Confirm that user.name and user.email are of class String. 
>> foo.name.class
=> String
>> foo.email.class
=> String

2.Of what class are the created_at and updated_at attributes?

>> foo.created_at.class
=> ActiveSupport::TimeWithZone
>> foo.updated_at.class
=> ActiveSupport::TimeWithZone
>> 

1.Find the user by name. Confirm that find_by_name works as well. (You will often encounter this older style of find_by in legacy Rails applications.) 

 User.find_by_name('Foo')
  User Load (0.2ms)  SELECT  "users".* FROM "users" WHERE "users"."name" = ? LIMIT ?  [["name", "Foo"], ["LIMIT", 1]]
=> #<User id: 1, name: "Foo", email: "foo@bar.com", created_at: "2017-04-18 16:30:31", updated_at: "2017-04-18 16:30:31">

2.For most practical purposes, User.all acts like an array, but confirm that in fact it’s of class User::ActiveRecord_Relation. 

>> m=User.all
  User Load (0.1ms)  SELECT "users".* FROM "users"
=> #<ActiveRecord::Relation [#<User id: 1, name: "Foo", email: "foo@bar.com", created_at: "2017-04-18 16:30:31", updated_at: "2017-04-18 16:30:31">]>
>> m.class
=> User::ActiveRecord_Relation

3.Confirm that you can find the length of User.all by passing it the length method (Section 4.2.3). Ruby’s ability to manipulate objects based on how they act rather than on their formal class type is called duck typing, based on the aphorism that “If it looks like a duck, and it quacks like a duck, it’s probably a duck.” 

>> m.length
=> 1

1.Update the user’s name using assignment and a call to save.

>> user=User.first
  User Load (0.1ms)  SELECT  "users".* FROM "users" ORDER BY "users"."id" ASC LIMIT ?  [["LIMIT", 1]]
=> #<User id: 1, name: "Foo", email: "foo@bar.com", created_at: "2017-04-18 16:30:31", updated_at: "2017-04-18 16:30:31">
>> user.name='teste'
=> "teste"
>> user.save
   (0.1ms)  SAVEPOINT active_record_1
  SQL (0.4ms)  UPDATE "users" SET "name" = ?, "updated_at" = ? WHERE "users"."id" = ?  [["name", "teste"], ["updated_at", 2017-04-18 16:39:55 UTC], ["id", 1]]
   (0.0ms)  RELEASE SAVEPOINT active_record_1
=> true
>> 

2.Update the user’s email address using a call to update_attributes.

>> user.update_attributes(email:"werzel_r@hotmail.com")
   (0.1ms)  SAVEPOINT active_record_1
  SQL (0.2ms)  UPDATE "users" SET "email" = ?, "updated_at" = ? WHERE "users"."id" = ?  [["email", "werzel_r@hotmail.com"], ["updated_at", 2017-04-18 16:40:55 UTC], ["id", 1]]
   (0.1ms)  RELEASE SAVEPOINT active_record_1
=> true
>> 

3.Confirm that you can change the magic columns directly by updating the created_at column using assignment and a save. Use the value 1.year.ago, which is a Rails way to create a timestamp one year before the present time.

>> user.created_at=1.year.ago
=> Mon, 18 Apr 2016 16:43:33 UTC +00:00
>> user.save
   (0.1ms)  SAVEPOINT active_record_1
  SQL (0.1ms)  UPDATE "users" SET "created_at" = ?, "updated_at" = ? WHERE "users"."id" = ?  [["created_at", 2016-04-18 16:43:33 UTC], ["updated_at", 2017-04-18 16:43:41 UTC], ["id", 1]]
   (0.0ms)  RELEASE SAVEPOINT active_record_1
=> true
>> 

1.In the console, confirm that a new user is currently valid.

>> user = User.new
=> #<User id: nil, name: nil, email: nil, created_at: nil, updated_at: nil>
>> user.valid?
=> true

2.Confirm that the user created in Section 6.1.3 is also valid. 

>> user = User.new(name: "Michael Hartl", email: "mhartl@example.com")
=> #<User id: nil, name: "Michael Hartl", email: "mhartl@example.com", created_at: nil, updated_at: nil>
>> user.valid?
=> true

1.Make a new user called u and confirm that it’s initially invalid. What are the full error messages?

>> u.errors.messages
=> {:name=>["can't be blank"], :email=>["can't be blank"]}
>>

2.Confirm that u.errors.messages is a hash of errors. How would you access just the email errors? 

 u.errors.messages.class
=> Hash
?> u.errors.messages[:email]
=> ["can't be blank"]
>> 

1.Make a new user with too-long name and email and confirm that it’s not valid.

>> user=User.new
=> #<User id: nil, name: nil, email: nil, created_at: nil, updated_at: nil>
>> user.name='adslkj;alkjfl;askdj;lakdfj;lakdsj;lkdaj;lkadfj;lkajdf;lkdfjl;fadkja;ldkjfd;alkja;lfkjl;fkdaj;ldafkjaf;ldkjfad;lkjfda;lkfadjl;fkdjfal;kjafdl;kjfda;l'
=> "adslkj;alkjfl;askdj;lakdfj;lakdsj;lkdaj;lkadfj;lkajdf;lkdfjl;fadkja;ldkjfd;alkja;lfkjl;fkdaj;ldafkjaf;ldkjfad;lkjfda;lkfadjl;fkdjfal;kjafdl;kjfda;l"
>> user.email='adslkj;alkjfl;askdj;lakdfj;lakdsj;lkdaj;lkadfj;lkajdf;lkdfjl;fadkja;ldkjfd;alkja;lfkjl;fkdaj;ldafkjaf;ldkjfad;lkjfda;lkfadjl;fkdjfal;kjafdl;kjfda;l'
=> "adslkj;alkjfl;askdj;lakdfj;lakdsj;lkdaj;lkadfj;lkajdf;lkdfjl;fadkja;ldkjfd;alkja;lfkjl;fkdaj;ldafkjaf;ldkjfad;lkjfda;lkfadjl;fkdjfal;kjafdl;kjfda;l"
>> user.valid?
=> false
>> 


2.What are the error messages generated by the length validation? 

>> user.errors.messages
=> {:name=>["is too long (maximum is 50 characters)"], :email=>["is too long (maximum is 255 characters)"]}

1.dd a test for the email downcasing from Listing 6.32, as shown in Listing 6.33. This test uses the reload method for reloading a value from the database and the assert_equal method for testing equality. To verify that Listing 6.33 tests the right thing, comment out the before_save line to get to red, then uncomment it to get to green. 

 FAIL["test_email_addresses_should_be_saved_as_lower-case", UserTest, 0.13723865000065416]
 test_email_addresses_should_be_saved_as_lower-case#UserTest (0.14s)
        Expected: "foo@example.com"
          Actual: "Foo@ExAMPle.CoM"
        test/models/user_test.rb:62:in `block in <class:UserTest>'

  9/9: [==================================] 100% Time: 00:00:00, Time: 00:00:00

Finished in 0.13884s
9 tests, 17 assertions, 1 failures, 0 errors, 0 skips

 9/9: [==================================] 100% Time: 00:00:00, Time: 00:00:00

Finished in 0.11332s
9 tests, 17 assertions, 0 failures, 0 errors, 0 skips

2.By running the test suite, verify that the before_save callback can be written using the “bang” method email.downcase! to modify the email attribute directly, as shown in Listing 6.34. 

before_save { email.downcase! }

 9/9: [==================================] 100% Time: 00:00:00, Time: 00:00:00

Finished in 0.09936s
9 tests, 17 assertions, 0 failures, 0 errors, 0 skips

1.Confirm that a user with valid name and email still isn’t valid overall.

user.valid?
  User Exists (1.1ms)  SELECT  1 AS one FROM "users" WHERE LOWER("users"."email") = LOWER(?) LIMIT ?  [["email", "mhartl@example.com"], ["LIMIT", 1]]
=> false
>> 

2.What are the error messages for a user with no password? 

>> user.errors.messages
=> {:password=>["can't be blank"]}

1.Confirm that a user with valid name and email but a too-short password isn’t valid.

>> user.password='123'
=> "123"
>> user.valid?
  User Exists (0.2ms)  SELECT  1 AS one FROM "users" WHERE LOWER("users"."email") = LOWER(?) LIMIT ?  [["email", "mhartl@example.com"], ["LIMIT", 1]]
=> false

2.What are the associated error messages? 
=> {:password=>["is too short (minimum is 6 characters)"]}

1.Quit and restart the console, and then find the user created in this section.


user = User.find_by_email('mhartl@example.com')
  User Load (0.6ms)  SELECT  "users".* FROM "users" WHERE "users"."email" = ? LIMIT ?  [["email", "mhartl@example.com"], ["LIMIT", 1]]
=> #<User id: 1, name: "Michael Hartl", email: "mhartl@example.com", created_at: "2017-04-18 17:55:13", updated_at: "2017-04-18 17:55:13", password_digest: "$2a$10$wwtAmIBcEHzDGt71CTj17eVG1m8nqyb1h3OC5PmsxaV...">
>> 

2.Try changing the name by assigning a new name and calling save. Why didn’t it work?
A senha nao foi setada.
user.errors.messages
=> {:password=>["can't be blank", "is too short (minimum is 6 characters)"]}

3.Update user’s name to use your name. Hint: The necessary technique is covered in Section 6.1.5. 

>> user.update_attributes(name:"ricardo",password: "123456", password_confirmation: "123456")
   (0.1ms)  begin transaction
  User Exists (0.2ms)  SELECT  1 AS one FROM "users" WHERE LOWER("users"."email") = LOWER(?) AND ("users"."id" != ?) LIMIT ?  [["email", "mhartl@example.com"], ["id", 1], ["LIMIT", 1]]
  SQL (2.1ms)  UPDATE "users" SET "name" = ?, "updated_at" = ?, "password_digest" = ? WHERE "users"."id" = ?  [["name", "ricardo"], ["updated_at", 2017-04-18 18:03:19 UTC], ["password_digest", "$2a$10$Q0tqpKuZlgTCt31wY3I6wuk3/2hTiU9zyjvE/aDrSFY8KZuwG4OBq"], ["id", 1]]
   (10.7ms)  commit transaction
=> true


CAPITULO 7

1.Visit /about in your browser and use the debug information to determine the controller and action of the params hash.

parameters: !ruby/hash:ActiveSupport::HashWithIndifferentAccess
  controller: static_pages


2.In the Rails console, pull the first user out of the database and assign it to the variable user. What is the output of puts user.attributes.to_yaml? Compare this to using the y method via y user.attributes.

>> puts user.attributes.to_yaml
---
id: 1
name: ricardo
email: mhartl@example.com
created_at: !ruby/object:ActiveSupport::TimeWithZone
  utc: &1 2017-04-18 17:55:13.974778000 Z
  zone: &2 !ruby/object:ActiveSupport::TimeZone
    name: Etc/UTC
  time: *1
updated_at: !ruby/object:ActiveSupport::TimeWithZone
  utc: &3 2017-04-18 18:03:19.112042000 Z
  zone: *2
  time: *3
password_digest: "$2a$10$Q0tqpKuZlgTCt31wY3I6wuk3/2hTiU9zyjvE/aDrSFY8KZuwG4OBq"
=> nil
>> user.attributes
=> {"id"=>1, "name"=>"ricardo", "email"=>"mhartl@example.com", "created_at"=>Tue, 18 Apr 2017 17:55:13 UTC +00:00, "updated_at"=>Tue, 18 Apr 2017 18:03:19 UTC +00:00, "password_digest"=>"$2a$10$Q0tqpKuZlgTCt31wY3I6wuk3/2hTiU9zyjvE/aDrSFY8KZuwG4OBq"}

user.attributes retorna uma hash enquanto o outro no formato yml

1.Using embedded Ruby, add the created_at and updated_at “magic column” attributes to the user show page from Listing 7.4.

<%= @user.name %>, <%= @user.email %>, <%= @user.created_at %>, <%= @user.updated_at %>

2.Using embedded Ruby, add Time.now to the user show page. What happens when you refresh the browser?

<%= @user.name %>, <%= @user.email %>, <%= @user.created_at %>, <%= @user.updated_at %>, <%= Time.now %>

Aparece o horario atual.

1.With the debugger in the show action as in Listing 7.6, hit /users/1. Use puts to display the value of the YAML form of the params hash. Hint: Refer to the relevant exercise in Section 7.1.1.1. How does it compare to the debug information shown by the debug method in the site template?
Put the debugger in the User new action and hit /users/new. What is the value of @user?With the debugger in the show action as in Listing 7.6, hit /users/1. Use puts to display the value of the YAML form of the params hash. Hint: Refer to the relevant exercise in Section 7.1.1.1. How does it compare to the debug information shown by the debug method in the site template?

(byebug) puts params.to_yaml
--- !ruby/object:ActionController::Parameters
parameters: !ruby/hash:ActiveSupport::HashWithIndifferentAccess
  controller: users
  action: show
  id: '1'
permitted: false

sao iguais.


2.Put the debugger in the User new action and hit /users/new. What is the value of @user?

puts @user

nil

1.Associate a Gravatar with your primary email address if you haven’t already. What is the MD5 hash associated with the image?

907cc4fbcbbdee0fe381778205e4ecde

2.Verify that the code in Listing 7.12 allows the gravatar_for helper defined in Section 7.1.4 to take an optional size parameter, allowing code like gravatar_for user, size: 50 in the view. (We’ll put this improved helper to use in Section 10.3.1.)

<%= gravatar_for @user, size: 50 %>


3.The options hash used in the previous exercise is still commonly used, but as of Ruby 2.0 we can use keyword arguments instead. Confirm that the code in Listing 7.13 can be used in place of Listing 7.12. What are the diffs between the two?

funciona, a diferenca que na 7.12 ẽ passado um array na 7.13 apenas um simbolo.

1.In Listing 7.15, replace :name with :nome. What error message do you get as a result?

undefined method `nome' for #<User:0x007fe3ab3283e0>
Did you mean?  name

2.Confirm by replacing all occurrences of f with foobar that the name of the block variable is irrelevant as far as the result is concerned. Why might foobar nevertheless be a bad choice?

<%= form_for(@user) do |foobar| %>
      <%= foobar.label :name %>
      <%= foobar.text_field :name %>

      <%= foobar.label :email %>
      <%= foobar.email_field :email %>

      <%= foobar.label :password %>
      <%= foobar.password_field :password %>

      <%= foobar.label :password_confirmation, "Confirmation" %>
      <%= foobar.password_field :password_confirmation %>

      <%= foobar.submit "Create my account", class: "btn btn-primary" %>

f alem de ser simples ainda diz como abreviacao de field      

1.Learn Enough HTML to Be Dangerous, in which all HTML is written by hand, doesn’t cover the form tag. Why not?


1.By hitting the URL /users/new?admin=1, confirm that the admin attribute appears in the params debug information.

parameters: !ruby/hash:ActiveSupport::HashWithIndifferentAccess
  admin: '1'
aparece


